default: all

guix.html: guix.md Makefile css/guix.css
	pandoc -f markdown-implicit_figures -t revealjs -V revealjs-url=. -V theme=solarized -V css=css/guix.css -o guix.html -s --slide-level 2 guix.md

guix2.html: guix2.md Makefile css/guix.css
	pandoc -f markdown-implicit_figures -t revealjs -V revealjs-url=. -V theme=solarized -V css=css/guix.css -o guix2.html -s --slide-level 2 guix2.md

guix3.html: guix3.md Makefile css/guix.css
	pandoc -f markdown-implicit_figures -t revealjs -V revealjs-url=. -V theme=solarized -V css=css/guix.css -o "$@" -s --slide-level 2 "$<"

guix4.html: guix4.md Makefile css/guix.css
	pandoc -f markdown-implicit_figures -t revealjs -V revealjs-url=. -V theme=solarized -V css=css/guix.css -o "$@" -s --slide-level 2 "$<"

all: guix.html guix2.html guix3.html guix4.html
